#include "isograms.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <malloc.h>

/**
    Check if the string is an isogram.
    Return true if it is, false otherwise.
**/
bool isIsogram(char* str)
{
    char* newStr = trim(str);

    if(str == NULL) return FALSE; // if str is a null pointer
    if(strlen(newStr) == 0) return TRUE; // if str == ""
    if(strlen(newStr) == 1 &&
       tolower(newStr[0]) >= 'a' && tolower(newStr[0]) <= 'z')
        return TRUE;

    bool result = checkCharacters(newStr);
    if(str != newStr) free(newStr);

    return result;
}

/**
    Check if there are redundant characters.
    Return true if there are none, false otherwise.
**/
bool checkCharacters(char* str)
{
    if(str == NULL) return FALSE;

    // build an array for all lower alphabet letters
    unsigned short countLetters[NB_UNIQUE_LETTERS] = {0};
    unsigned int strLength = strlen(str);
    for(int i = 0; i < strLength; i++)
    {
        char currentChar = tolower(str[i]); // lowering current character
        if(currentChar >= 'a' && currentChar <= 'z')
        {
            // using acsii code to find position in array ('a' = 97)
            if(countLetters[currentChar - 'a'] > 0) return FALSE;
            countLetters[currentChar - 'a'] = 1;
        }
        // character unsupported
        else return FALSE;
    }

    return TRUE;
}

/**
    Remove leading and trailing spaces from a string.
    If none are found, return the current string.
    Else, return a new trimmed string
**/
char* trim(char* str)
{
    if(str == NULL) return str;

    unsigned int charPosition = 0, startStr = 0, endStr = 0;

    while(str[charPosition] == ' ') charPosition++;
    startStr = charPosition;
    if(charPosition == strlen(str)) return ""; // means that the string contains only spaces

    charPosition = strlen(str) - 1;
    while(str[charPosition] == ' ') charPosition--;
    endStr = charPosition;

    if(startStr > 0 || endStr < strlen(str) - 1)
        return substring(str, startStr, endStr + 1);

    return str;
}

/**
    Return a new string starting and ending at the specified positions
**/
char* substring(char* str, unsigned int startPos, unsigned int endPos)
{
    if(str == NULL) return str;
    if(startPos < 0 || endPos < startPos || endPos > strlen(str)) return str;

    char* newStr = (char*) calloc(endPos - startPos + 1, sizeof(char));
    if(newStr != NULL)
        for(unsigned int i = startPos, j = 0; i < endPos; i++, j++)
            newStr[j] = str[i];
    else exit(20);

    return newStr;
}

