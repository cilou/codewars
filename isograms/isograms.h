#ifndef ISOGRAMS_H_
#define ISOGRAMS_H_

#define NB_UNIQUE_LETTERS 26

typedef unsigned short bool;
#define TRUE  1
#define FALSE 0

bool isIsogram(char* str);
bool checkCharacters(char* str);
char* trim(char* str);
char* substring(char* str, unsigned int startPos, unsigned int endPos);

#endif // ISOGRAMS_H_


