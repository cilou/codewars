#include <stdio.h>
#include <stdlib.h>
#include "isograms.h"
#include "assert.h"

void tests()
{
    assert(isIsogram(NULL) == 0);

    assert(isIsogram("") == 1);
    assert(isIsogram(" ") == 1);
    assert(isIsogram("    ") == 1);

    assert(isIsogram("azerty") == 1);
    assert(isIsogram("AzErTy") == 1);
    assert(isIsogram("azeErty") == 0);

    assert(isIsogram(" aZeRty ") == 1);
    assert(isIsogram("  aZeRty   ") == 1);
    assert(isIsogram(" aZeRty") == 1);
    assert(isIsogram("aZeRty ") == 1);

    assert(isIsogram(" aZe Rty ") == 0);
    assert(isIsogram(" aZe Rty") == 0);
    assert(isIsogram("aZe Rty ") == 0);
}

int main()
{
    tests();
    printf("%d", isIsogram("Dermatoglyphics"));
    return 0;
}


